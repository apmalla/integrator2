#### Variables for AWS Region ####
variable "aws_region" {
  default = "us-east-1"
}

variable "vpc_name" {
  default = "test"
}

variable "cidr" {
  default = "10.10.0.0/16"
}

variable "route53_zone_name" {
  default = "internal.test.com"
}

#### Variable for SSH Key Pair ####
variable "pub_key_path" {
  default = "~/.ssh/id_rsa.pub"
}

#### Variable for Public Subnet1 ####
variable "pub_sn_aza_cidr" {
  default = "10.10.0.0/20"
}

variable "pub_sn_aza_name" {
  default = "pub_sub_a"
}

#### Variable for Public Subnet2 ####
variable "pub_sn_azb_cidr" {
  default = "10.10.16.0/20"
}

variable "pub_sn_azb_name" {
  default = "pub_sub_b"
}

#### Variable for Private Subnet1 ####
variable "priv_sn_aza_cidr" {
  default = "10.10.32.0/20"
}

variable "priv_sn_aza_name" {
  default = "priv_sub_a"
}

#### Variable for Private Subnet2 ####
variable "priv_sn_azb_cidr" {
  default = "10.10.48.0/20"
}

variable "priv_sn_azb_name" {
  default = "priv_sub_b"
}

#### Variable for DB Private Subnet1 ####
variable "db_sn_aza_cidr" {
  default = "10.10.64.0/20"
}

variable "db_sn_aza_name" {
  default = "db_sn_a"
}

#### Variable for DB Private Subnet2 ####
variable "db_sn_azb_cidr" {
  default = "10.10.80.0/20"
}

variable "db_sn_azb_name" {
  default = "db_sn_b"
}

#### Variables for ALB ####
variable "alb_name" {
  default = "test-alb"
}

#### Variables for DB ALB ####
variable "db_alb_name" {
  default = "db-alb"
}

#### Variables for Launch Configuration ####
variable "launch_configuration_name" {
  default = "test"
}

variable "lc_name" {
  default = "lc"
}

variable "image_id" {
    type = "map"
    default = {
      us-east-1 = "ami-66506c1c"
}
}

variable "instance_type" {
  default = "t2.micro"
}

variable  "aws_region_os" {
  default = ""
}

#### Variable for Autoscaling Group ####
variable "key" {
  default = "Environment"
}

variable "value" {
  default = "Dev"
}

