provider "aws" {
  region = "${var.aws_region}"
}

module "vpc" {
  source            = "git::ssh://git@github.com/opstree-terraform/vpc"
  cidr              = "${var.cidr}"
  name              = "${var.vpc_name}"
  route53_zone_name = "${var.route53_zone_name}"
}

module "key_pair" {
  source          = "git::ssh://git@github.com/opstree-terraform/key_pair"
  public_key_path = "${var.pub_key_path}"
  name            = "${var.vpc_name}-key"
}

module "security_group" {
  source          = "git::ssh://git@github.com/opstree-terraform/pub_ssh_sg"
  vpc_id          = "${module.vpc.id}"
}

module "pub_sn_a" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet"
  vpc_id                  = "${module.vpc.id}"
  cidr                    = "${var.pub_sn_aza_cidr}"
  az                      = "${var.aws_region}a"
  name                    = "${var.pub_sn_aza_name}"
  map_public_ip_on_launch = "false"
}

module "pub_sn_b" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet"
  vpc_id                  = "${module.vpc.id}"
  cidr                    = "${var.pub_sn_azb_cidr}"
  az                      = "${var.aws_region}b"
  name                    = "${var.pub_sn_azb_name}"
  map_public_ip_on_launch = "false"
}

module "priv_sn_a" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet"
  vpc_id                  = "${module.vpc.id}"
  cidr                    = "${var.priv_sn_aza_cidr}"
  az                      = "${var.aws_region}a"
  name                    = "${var.priv_sn_aza_name}"
}

module "priv_sn_b" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet"
  vpc_id                  = "${module.vpc.id}"
  cidr                    = "${var.priv_sn_azb_cidr}"
  az                      = "${var.aws_region}b"
  name                    = "${var.priv_sn_azb_name}"
}

module "db_sn_a" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet"
  vpc_id                  = "${module.vpc.id}"
  cidr                    = "${var.db_sn_aza_cidr}"
  az                      = "${var.aws_region}a"
  name                    = "${var.db_sn_aza_name}"
}

module "db_sn_b" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet"
  vpc_id                  = "${module.vpc.id}"
  cidr                    = "${var.db_sn_azb_cidr}"
  az                      = "${var.aws_region}b"
  name                    = "${var.db_sn_azb_name}"
}

module "priv_route_table" {
  source         = "git::ssh://git@github.com/opstree-terraform/pvt_route_table"
  vpc_id                  = "${module.vpc.id}"
  pub_sn_id               = "${module.pub_sn_a.id}"
  vpc_name                = "${var.vpc_name}"
}

module "pub_sn_a_association" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet_association"
  subnet_id              = "${module.pub_sn_a.id}"
  route_table_id         = "${module.vpc.public_route_table_id}"
}

module "pub_sn_b_association" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet_association"
  subnet_id              = "${module.pub_sn_b.id}"
  route_table_id         = "${module.vpc.public_route_table_id}"
}

module "priv_sn_a_association" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet_association"
  subnet_id              = "${module.priv_sn_a.id}"
  route_table_id         = "${module.priv_route_table.route_table_id}"
}

module "priv_sn_b_association" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet_association"
  subnet_id              = "${module.priv_sn_b.id}"
  route_table_id         = "${module.priv_route_table.route_table_id}"
}

module "db_sn_a_association" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet_association"
  subnet_id              = "${module.db_sn_a.id}"
  route_table_id         = "${module.priv_route_table.route_table_id}"
}

module "db_sn_b_association" {
  source         = "git::ssh://git@github.com/opstree-terraform/subnet_association"
  subnet_id              = "${module.db_sn_b.id}"
  route_table_id         = "${module.priv_route_table.route_table_id}"
}

module "web_security_group" {
  source          = "git::ssh://git@github.com/opstree-terraform/pub_web_sg"
  vpc_id          = "${module.vpc.id}"
}

module "alb" {
  source                        = "git::ssh://git@github.com/opstree-terraform/alb"
  alb_name                      = "${var.alb_name}"
  alb_security_groups           = ["${module.web_security_group.id}"]
#   certificate_arn               = "arn:aws:iam::123456789012:server-certificate"
  health_check_path             = "/login"
  subnets                       = ["${module.priv_sn_a.id}", "${module.priv_sn_b.id}"]
  tags                          = "${map("Environment", "test")}"
  vpc_id                        = "${module.vpc.id}"
}

module "db-alb" {
  source                        = "git::ssh://git@github.com/opstree-terraform/alb"
  alb_name                      = "${var.db_alb_name}"
  alb_security_groups           = ["${module.web_security_group.id}"]
#   certificate_arn               = "arn:aws:iam::123456789012:server-certificate"
  health_check_path             = "/login"
  subnets                       = ["${module.db_sn_a.id}", "${module.db_sn_b.id}"]
  tags                          = "${map("Environment", "test")}"
  vpc_id                        = "${module.vpc.id}"
}

module "aws_launch_configuration" {
  source               = "git::ssh://git@github.com/opstree-terraform/launch_configuration"
  name                 = "${var.launch_configuration_name}"
  lc_name              = "${var.lc_name}"
  image_id             = "${var.image_id}"
  aws_region           = "${var.aws_region}"
  key_name             = "${module.key_pair.id}"
  instance_type        = "${var.instance_type}"
  security_groups      = ["${module.security_group.id}"]
  root_volume_type     = "gp2"
  root_volume_size     = "10"
}

module "asg" {
  source = "git::ssh://git@github.com/opstree-terraform/autoscaling_group"
  name                      = "${var.vpc_name}"
  asg_name                  = "${var.vpc_name}-asg"
  vpc_zone_identifier       = ["${module.pub_sn_a.id}", "${module.pub_sn_b.id}", "${module.db_sn_a.id}", "${module.db_sn_b.id}"]
  launch_configuration      = "${module.aws_launch_configuration.this_launch_configuration_id}"
#   load_balancers            = ["${var.alb_name}", "${var.db_alb_name}"]
  target_group_arns         = ["${module.alb.target_group_arn}", "${module.db-alb.target_group_arn}"]
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 2
  desired_capacity          = 2
  wait_for_capacity_timeout = 0
  key                       = "${var.key}"
  value                     = "${var.value}"
}
